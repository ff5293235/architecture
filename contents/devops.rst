=======================================
DevOps in the Cloud, Deploy at the Edge
=======================================
The DevOps methodology is well established for cloud workflows. There is a huge opportunity for cloud development workflows and practices to be re-used in the automotive industry. 
This is not just about how to do continuous testing/building in the cloud or automated deployments on edge devices but it also implies how end-user applications will be developed. Adopting a DevOps model in automotive implies that most embedded software developers will no longer need to develop, build, and test applications using a host development system alongside a target/edge platform for testing. 
While it is possible to inherit most DevOps practices from a cloud-based environment, deploying systems and applications on edge devices have its unique set of challenges. A few of these challenges are listed below: 

* When and how should application updates be deployed? 
* How will the system deal with an update of a running application?
* How to identify if an application has enough resources to run, keeping in mind it is not limited to the usual computing resources but also car features/components.
* How will I test applications in an automated manner in an emulated environment in the cloud. 

Essential to support application development in the cloud is the requirement for a base operating system that can run not only in the cloud for testing but must also run on the edge platform for on-device testing. Sticking to well-known cloud technologies such as OCI compatible containers are essential in developing/testing applications in the cloud, allowing the same portable container to also be deployed at the edge for example, in a 
vehicle. To help facilitate this workflow and provide repeatable examples, the SOAFEE SIG has introduced SOAFEE Blueprints and the SOAFEE Integration Lab. 

SOAFEE Blueprints 
-----------------
SOAFEE Blueprints are example reference application software solutions guided by specific automotive software defined use-cases used to validate SOAFEE architectural concepts and accelerate product development and prototyping. Blueprints are intended to demonstrate concepts such as DevOps in the Cloud and Deploy at the Edge, however it is not a requirement for a blueprint to cover the complete end-to-end DevOps cycle.. This will allow SOAFEE members to validate application-level use-cases and showcase edge deployments. 

SOAFEE Integration Lab
----------------------
SOAFEE Blueprints will be key to validating the SW defined model defined by SOAFEE. Blueprints will be used to validate the cloud native development, integration and test workflows in the cloud. Once the cloud native development workflow has been developed/matured for the blueprint the next step is to test/validate on the edge compute platforms. This is where SOAFEE Integration Labs will play an important role. This is a lab that will make available supported edge platforms for on-device testing of workloads. These labs provide the hardware to test the “Deploy at the Edge” workflow on embedded edge hardware. 
The Integration Lab will enable the testing and integration of compliant hardware platforms, both virtual and physical, with SOAFEE compliant implementations such as the the Edge Workload Abstraction & Orchestration Layer (EWAOL), a SOAFEE reference implementation, and stacks from commercial software vendors, with workloads such as Open AD Kit that conform to the SOAFEE architecture as outlined in this document. 
The combination of the SOAFEE Blueprints and the SOAFEE Integration Lab will create a quick start tool for third parties to build functional solutions quickly and easily on SOAFEE compliant solutions.
